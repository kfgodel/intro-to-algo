Introduction to algorithms
==============

This applications implements algorithms and examples from the book https://mitpress.mit.edu/books/introduction-algorithms
This is a sandbox for testing the examples and measuring performance