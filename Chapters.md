## Chapters

Super-abstract of each chapter

#### 1. Foundations
Quick introduction and overview on the concept of algorithms
- Examples and cases

#### 2. Getting started
Analysis of algorithms using `Insertion sort` as the first one (worst in-place).
- Worst case
- Average case
- Divide and conquer approach
Alternative sort with `Merge sort` (requires memory for copy)
- Recurrence equation to analyze recursive algorithms (follows in ch 4)

#### 3. Growth of Functions
O-notation and alternatives. Asymptotic analysis.
![Curves](https://adrianmejia.com/images/time-complexity-examples.png)
Bunch of math to estimate growth.
- Double bound: θ-notation
- Upper bound:
  - Tight bound: Ο-notation
  - Not tight: o-notation
- Lower bound:
  - Tight: Ω-notation
  - not tight: ω-notation

#### 4. Divide and Conquer
Steps: Divide, Conquer, Combine
- Maximum subarray problem
- Strassen's algorithm for matrix manipulation
Methods for solving recurrences
- Substitution for recurrences
- Recursion tree for recurrences
- Master method for recurrences

#### 5. Probabilistic analysis and randomized algorithms
Probabilistic analysis with the hiring problem
- Randomized algorithm (depends on random number as well as input)
- Indicator random variable

`Permute by sorting`, `Randomize in place`
More math in form of probabilistic analysis

#### 6. Sorting and order statistics
Sorting algorithm for a sequence of numbers:
- `Heapsort`: Good for in-place (not better than q-sort on average)
  - `Max-heap`: For important first
  - `Min-heap`: For early first
- `Priority queues`: Uses heapsort to order tasks


#### 7. Quicksort
Description, performance, randomized version
- `Q-Sort`: Defines a pivot element to dived and conquer recursively

#### 8. Sorting in linear time
- `Counting sort`: Assumes known range of integers
- `Radix sort`: Orders by digits of each number
- `Bucket sort`: Assumes input in the interval [0,1), splits into buckets. Requires lists and another sort

#### 9. Medians and Order statistics
Minimum and maximum
- `Randomized select`. Kind of Q-sort where we look for the i-th element in one of the partitions

#### 10. Elementary data structures
`Stacks`, `queues`, `linked lists`, `rooted trees`.
Single array representation.

#### 11. Hash tables
- Direct address tables: use values as indexes (when value universe is small)
- Hash functions: each key is equally likely assigned to any slot
  - division method: k mod m
  - multiplication method: floor(m * (k*A mod 1.0)). A=(sqr(5) - 1) / 2
- Universal hashing: random select a hash function
- Open addressing: Collistion solving. Probe sequence to find empty slot. Requires delete mark
  - linear probing: look for next slot. bad clustering best performance
  - quadratic: look for cuadratic of next slot. average
  - doble hashing: use a 2nd has for slot. good clusteing, bad performance

#### 12. Binary search trees
`Inorder tree walk`. Search, minimum, maximum, insertion, deletion.
Randomly built binary search trees.

#### 13. Red-black trees
- Red black tree: Binary balanced 3 with 4 restrictions. Ensures longest y at most 2x shortest path
  - Properties, rotations, insertion, deletion
- Persistent dynamic set: root copy with only changed nodes, and rest point to existing
- AVL trees: Strict balance dif between shortest and longest can be 1
- Treaps: key and priority. Order of keys, min-heap priorities

Extra help from videos:
[Trees](https://www.youtube.com/watch?v=hmSFuM2Tglw), [AVL trees](https://www.youtube.com/watch?v=Jj9Mit24CWk)

#### 14. Augmenting data structures
Dynamic order statistics, retrieve by rank, maintain subtree sizes.
How to augment data structures.
- Interval trees: RB trees

#### 15. Dynamic programming
Algorithm + table of previous steps. Find the best combination, by reusing previous calculus.
Problems share sub-problems. Usually optimization problems.

- Optimal substructure: an optimal solution contains optimal solutions to sub-problems
  Considerations

1. Characterize structure for optimal solution
  - An optimal solution contains optimal solutions to sub-problems
2. Recursively define value of an optimal
3. Compute the value of an optimal (usually bottom-up)
4. Construct optimal solution for computed information

Top-down memoization, bottom-up method. Sub-problem graph
Matrix chain multiplication. Optimal substructure.
Longest common subsequence.
- Optimal binary search tree: probability of search per node

Extra help:
[Short explanation](https://www.youtube.com/watch?v=vYquumk4nWw)
[Alternative explanation of the method](https://www.youtube.com/watch?v=aPQY__2H3tE)
1. Visualize examples
2. Find am appropriate sub-problem
3. Find the relationships among sub-problems
4. Generalize the relationship
5. Implement by solving sub-problems in order

#### 16. Greedy algorithms
Dynamic programing without evaluating all alternatives. Local optimal to reach global optimal.
Decide first, solve sub-problems later (reverse to dynamic programming)
1. Take a piece of the problem
2. Pick what looks like the best solution for that piece
3. Iterate with the rest of the sub-problem
---
1. Define the optimization problem so that a decision is made and a subproblem is left to solve
2. Prove that there's always an optimal solution that includes the greedy choice
3. Demonstrate optimal substructure so that after making the greedy choice, the subproblem + the choice is an optimal solution

- `Activity selection problem`: which task to run next
- `Knapsack problem`: steal as much value, considering bag capacity
- `Huffman codes`: Replace characters by bit representation. Lowest bits to highest appearing char
- `Matroids`: Greedy is good for finding optimal independent subset.
Task scheduling problem as matroid

Extra help:
[In a nutshell](https://www.youtube.com/watch?v=HzeK7g8cD0Y)


#### 17. Amortized Analysis
Guarantees the average performance of each operation in the worst case.
It helps on gaining insight into a particular data structure to optimize design.
- `Aggregate Analysis`: Calculate upper bound and divide by n operations
- `Accounting method`: Amortize more for some operations types that compensate other types
- `Potential method`: Credit as a whole in the structure instead of per type. Didn't get it

Dynamic tables: example analyzing cost of expansion and reduction

#### 18. B-Trees
Balanced search trees to work well on physical disks.
Number of disk access proportional to height. Root can be kept on memory as index for other nodes
- `B+Trees`: Stores data only on leaf, maximizing spaces for branches
Insertion may require splitting nodes in half when full, up to root node (which is then rotated).

Extra help
[Visual explanation](https://www.youtube.com/watch?v=SI6E4Ma2ddg)

#### 19. Fibonacci heaps
A mergeable heap with constant time operations.
Useful for algorithms on graphs if extract-min and delete not used too often.
List of heap trees where each tree has double linked lists of siblings.
Used for priority queues (better amortized than binomial heap and binary heap).
It's practical performance use is debated over the simple binary heaps

- Binomial heap: Root of order N has n trees of order n-1.
[Binomial Heap](https://www.youtube.com/watch?v=hbf_h8Ytv04)


#### 20. van Emde Boas trees
For integer keys in the range 0 to u-1 (no duplicates), where is power of 2.
Used on routers over ips.

#### 21. Data structures for disjoint sets
Used for finding connected nodes of undirected graph.
Operations
- make set, find, union
Implementation example using linked list.
- `Disjoint-set forest`: Implement sets as trees. Each node points to parent (root to itself)
  - `union by rank`: Optimization keeping a rank or height on every node (lowest is root)

### Graphs
#### 22. Elementary graph algorithms
Representation:
- `Adjacency-list`: for sparse graphs (more nodes). Default for book algorithms
  - 1 list per vertex, containing adjacent vertex
- `Adjacency-matrix`: for dense graphs (many edges, focus on edges)
  - a VxV matrix where each cell is the edge connecting them (directed or undirected)

- `Breadth first search`: Creates a tree from node, closer nodes first
  - Useful for finding shortest path from node
- `Depth first search`: Forest of trees connected. Uses discovery and finish time for intervals
  - Can be used to classify edges: forward, back, cross and tree edges
  - Can be used for `Topological sort`
    - Implemented as dfs where we insert vertex first in list as it is finished

`Topological sort`: For acyclic directed graph. A list of vertices where
  the vertex that appears after has an edge that points to it from a vertex that appears before.
  Source vertices appear before destination vertices. (order unspecified for siblings)

Strongly connected Components (bidirectional edges)
How to find connected components
- Do dfs and first on G
- Do dfs on transposed G in decreasing finish time order (from first dfs)
- Each resulting tree is a component

#### 23. Minimum spanning trees
Spanning Tree: Tree that connects all vertices of a graph
Look for spanning tree with minimum weight on edges for a connected graph

- Kruskal's algorithm
  - Create 1 tree per vertex. Safe edge is the least weight that connects to a new tree.
    Unite trees after connection

- Prim's algorithm
  - Updates only one tree while processing. Keeps track of lowest weight to unvisited
  - Start with a random vertex. Update table of lowest weight to adjacent new vertices
    using info from added vertex. Pick the next lowest weight vertex, and repeat
  - Each added vertex updates priority queue so the nearest vertex is chosen next

`Bottleneck spanning tree`: largest edge is minimum on all spanning trees

Extra help
[Prism explained](https://www.youtube.com/watch?v=eB61LXLZVqs)

#### 24. Single source shortest path
Find the shortest path from a source vertex (to every other vertex).
- Shortest path: has no cycles.
  - It can be represented as a tree. From source to all other vertices
- Negative weight and cycles: Have no shortest as each cycle may decrease "length" or cost.

- `Relaxation`: Assuming all vertices start with distance infinite to source. The algorithms
  relax that distance by reducing them iteratively. Dijkstra does so in only 1 iteration.
  Floyd-Warshall needs N-1 iterations.

- `Bellman-Ford algorithm`: general case usable with negative weights.
  - Iterate at most V-1 times, and for each iteration updates vertex distance to source
    using edge length and predecessor distance. Starting with infinite distance, if
    coming from predecessor is shortest, use that distance. Algorithm may converge before all iterations
- `Shortes path for DAGs`: In directed acyclic graphs a quick version of Bellman-Ford can be achieved
  by sorting topologically first. In that case only 1 iteration is needed.
  - Can be used to solve PERT chart `critical path` by negating weights
- `Dijkstra`: Shortest path between a pair of vertices (source and destination).
  -
- Floyd-Warshall: Shortest path between all pairs of vertices

Extra help
[Bellman-Ford](https://www.youtube.com/watch?v=obWXjtg0L64)
