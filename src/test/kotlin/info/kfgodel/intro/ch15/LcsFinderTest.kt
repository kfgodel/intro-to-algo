package info.kfgodel.intro.ch15

import info.kfgodel.jspek.api.JavaSpecRunner
import info.kfgodel.jspek.api.KotlinSpec
import org.assertj.core.api.Assertions.assertThat
import org.junit.runner.RunWith

/**
 * Date: 30/8/20 - 18:48
 */
@RunWith(JavaSpecRunner::class)
class LcsFinderTest : KotlinSpec() {
  override fun define() {
    describe("a LcsFinder") {
      val finder by let { LcsFinder() }

      it("finds no sequence if one of the inputs is empty"){
        val lcs = finder().findBetween("ABC","")
        assertThat(lcs).isEmpty()
      }

      it("finds the longest common sequence between 2 other sequences"){
        val lcs = finder().findBetween("ABCBDAB","BDCABA")
        assertThat(lcs).isEqualTo("BCBA")
      }
    }
  }

}
