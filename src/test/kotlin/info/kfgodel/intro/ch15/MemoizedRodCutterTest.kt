package info.kfgodel.intro.ch15

import info.kfgodel.jspek.api.JavaSpecRunner
import info.kfgodel.jspek.api.KotlinSpec
import org.assertj.core.api.Assertions.assertThat
import org.junit.runner.RunWith

/**
 * Date: 6/5/20 - 00:49
 */
@RunWith(JavaSpecRunner::class)
class MemoizedRodCutterTest : KotlinSpec() {
  override fun define() {
    describe("a memoized rod cutter") {
      val cutter by let { MemoizedRodCutter() }

      it("estimates the best revenue for a rod defined by its lenght ") {
        val cuttingPlan = cutter().cut(4)
        verifyThat(cuttingPlan, hasRevenue = 10, andParts = arrayOf(2, 2))
      }

      it("estimates revenue according to textbook examples pg.362") {
        verifyThat(cutter().cut(1), hasRevenue = 1, andParts = arrayOf(1))
        verifyThat(cutter().cut(2), hasRevenue = 5, andParts = arrayOf(2))
        verifyThat(cutter().cut(3), hasRevenue = 8, andParts = arrayOf(3))
        verifyThat(cutter().cut(4), hasRevenue = 10, andParts = arrayOf(2,2))
        verifyThat(cutter().cut(5), hasRevenue = 13, andParts = arrayOf(2,3))
        verifyThat(cutter().cut(6), hasRevenue = 17, andParts = arrayOf(6))
        verifyThat(cutter().cut(7), hasRevenue = 18, andParts = arrayOf(1,6))
        verifyThat(cutter().cut(8), hasRevenue = 22, andParts = arrayOf(2,6))
        verifyThat(cutter().cut(9), hasRevenue = 25, andParts = arrayOf(3,6))
        verifyThat(cutter().cut(10), hasRevenue = 30, andParts = arrayOf(10))
      }

      it("can estimate revenue for rod longer than 10"){
        verifyThat(cutter().cut(25), hasRevenue = 73, andParts = arrayOf(2, 3, 10, 10))
      }
    }
  }

  private fun verifyThat(plan: CuttingPlan, hasRevenue: Int, andParts: Array<Int>) {
    assertThat(plan.revenue).isEqualTo(hasRevenue)
    assertThat(plan.parts).isEqualTo(andParts)
  }
}
