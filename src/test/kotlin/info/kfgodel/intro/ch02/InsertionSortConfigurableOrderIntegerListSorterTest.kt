package info.kfgodel.intro.ch02

import info.kfgodel.jspek.api.JavaSpecRunner
import info.kfgodel.jspek.api.KotlinSpec
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.util.Lists
import org.junit.runner.RunWith

/**
 * Verifica que se haya implementado bien el algoritmo con la posibilida de invertir el orden
 * Created by kfgodel on 04/07/17.
 */
@RunWith(JavaSpecRunner::class)
class InsertionSortConfigurableOrderIntegerListSorterTest : KotlinSpec() {
  override fun define() {
    describe("a Q-Sort int array sorter") {
      val sorter by let { InsertionSortConfigurableOrderIntegerListSorter.create { elementoAnterior: Int, elementoReordenado: Int -> elementoAnterior <= elementoReordenado } }

      it("sorts the example array from the book pg 18") {
        val lista: List<Int> = Lists.newArrayList(5, 2, 4, 6, 1, 3)
        val sorted: List<Int> = sorter().sort(lista)
        assertThat(sorted).isEqualTo(Lists.newArrayList(1, 2, 3, 4, 5, 6))
      }

      it("returns an empty array if one given") {
        val lista: List<Int> = Lists.newArrayList()
        val sorted: List<Int> = sorter().sort(lista)
        assertThat(sorted).isEqualTo(Lists.newArrayList<Any>())
      }

      describe("when reverse order is given") {
        sorter { InsertionSortConfigurableOrderIntegerListSorter.create { elementoAnterior: Int, elementoReordenado: Int -> elementoAnterior >= elementoReordenado } }

        it("sorts the example array from the book pg 18 reversed") {
          val lista: List<Int> = Lists.newArrayList(5, 2, 4, 6, 1, 3)
          val sorted: List<Int> = sorter().sort(lista)
          assertThat(sorted).isEqualTo(Lists.newArrayList(6, 5, 4, 3, 2, 1))
        }

        it("returns an empty array if one given") {
          val lista: List<Int> = Lists.newArrayList()
          val sorted: List<Int> = sorter().sort(lista)
          assertThat(sorted).isEqualTo(Lists.newArrayList<Any>())
        }
      }
    }
  }
}
