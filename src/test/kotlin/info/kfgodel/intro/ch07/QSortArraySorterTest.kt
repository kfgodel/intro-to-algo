package info.kfgodel.intro.ch07

import info.kfgodel.jspek.api.JavaSpecRunner
import info.kfgodel.jspek.api.KotlinSpec
import org.assertj.core.api.Assertions.assertThat
import org.junit.runner.RunWith

/**
 * Verifica que se haya implementado bien el algoritmo
 * Created by kfgodel on 04/07/17.
 */
@RunWith(JavaSpecRunner::class)
class QSortArraySorterTest : KotlinSpec() {
  override fun define() {
    describe("a Q-Sort int array sorter") {
      val sorter by let { QSortIntArraySorter() }

      it("sorts the example array from the book pg 18") {
        val array = intArrayOf(5, 2, 4, 6, 1, 3)
        val sorted: IntArray = sorter().sort(array)
        assertThat(sorted).isEqualTo(intArrayOf(1, 2, 3, 4, 5, 6))
      }

      it("returns an empty array if one given") {
        val array = intArrayOf()
        val sorted: IntArray = sorter().sort(array)
        assertThat(sorted).isEqualTo(intArrayOf())
      }
    }
  }
}
