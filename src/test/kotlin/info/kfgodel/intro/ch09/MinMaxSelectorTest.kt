package info.kfgodel.intro.ch09

import info.kfgodel.jspek.api.JavaSpecRunner
import info.kfgodel.jspek.api.KotlinSpec
import org.assertj.core.api.Assertions.assertThat
import org.junit.runner.RunWith

/**
 * Verifica que se haya implementado bien el algoritmo
 * Created by kfgodel on 04/07/17.
 */
@RunWith(JavaSpecRunner::class)
class MinMaxSelectorTest : KotlinSpec() {
  override fun define() {
    describe("a min-max selector") {
      val selector by let { TBMinMaxSelector() }

      it("selects the minimum and maximum numbers from example array from the book pg 18") {
        val array = intArrayOf(5, 2, 4, 6, 1, 3)
        val (min, max) = selector().select(array)
        assertThat(min).isEqualTo(1)
        assertThat(max).isEqualTo(6)
      }

      itThrows(IllegalArgumentException::class.java, "if array is empty", {
        val array = intArrayOf()
        selector().select(array)
      }) { e ->
        assertThat(e).hasMessage("at least one element is needed")
      }
    }
  }
}
