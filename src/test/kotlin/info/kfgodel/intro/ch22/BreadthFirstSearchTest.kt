package info.kfgodel.intro.ch22

import info.kfgodel.jspek.api.JavaSpecRunner
import info.kfgodel.jspek.api.KotlinSpec
import org.assertj.core.api.Assertions.assertThat
import org.junit.runner.RunWith

/**
 * This test verifies that teh algorithm is correctly implemented with graph from page 596
 * Date: 30/8/20 - 18:48
 */
@RunWith(JavaSpecRunner::class)
class BreadthFirstSearchTest : KotlinSpec() {
  val graph = TBGraph()
    .connecting(1, 2, 3)
    .connecting(2, 1)
    .connecting(3, 1, 4)
    .connecting(4, 3, 5, 6)
    .connecting(5, 4, 6, 7)
    .connecting(6, 4, 5, 7, 8)
    .connecting(7, 5, 6, 8)
    .connecting(8, 6, 7)

  override fun define() {
    describe("a breadth first seach") {

      it("creates a tree using selected node as root and following closest neighbors first") {
        val resultTree = graph.bfs(3)
        assertThat(resultTree).isEqualTo(
          ComparableTree()
            .connecting(3, 1, 4)
            .connecting(1, 2)
            .connecting(4, 5, 6)
            .connecting(5, 7)
            .connecting(6, 8)
        )
      }
    }

  }
}
