package info.kfgodel.intro.ch13

import info.kfgodel.jspek.api.JavaSpecRunner
import info.kfgodel.jspek.api.KotlinSpec
import org.assertj.core.api.Assertions.assertThat
import org.junit.runner.RunWith

/**
 * Date: 6/5/20 - 00:49
 */
@RunWith(JavaSpecRunner::class)
class TextbookRBTreeTest : KotlinSpec() {
  override fun define() {
    describe("a red-black tree") {
      val tree by let { TextbookRBTree<Int>() }

      it("se crea vacio") {
        assertThat(tree().size()).isEqualTo(0)
      }

      it("permite la insercion de 1 elemento") {
        tree().insert(10)
        assertThat(tree().size()).isEqualTo(1)
      }

      describe("dado un estado inicial") {
        beforeEach {
          tree()
            .insert(10)
            .insert(20)
            .insert(30)
            .insert(40)
            .insert(50)
            .insert(15)
            .insert(18)
            .insert(25)
            .insert(38)
        }

        it("re-balancea el arbol al agregar nuevos elementos") {
          assertThat(tree().leftCount()).isEqualTo(3)
          assertThat(tree().rightCount()).isEqualTo(5)

          tree().insert(28)

          assertThat(tree().leftCount()).isEqualTo(6)
          assertThat(tree().rightCount()).isEqualTo(3)
        }

        it("re-balancea el arbol al quitar elementos") {
          assertThat(tree().leftCount()).isEqualTo(3)
          assertThat(tree().rightCount()).isEqualTo(5)

          tree().delete(20)

          assertThat(tree().leftCount()).isEqualTo(3)
          assertThat(tree().rightCount()).isEqualTo(4)
        }
      }
    }
  }
}
