package info.kfgodel.intro.ch10

import info.kfgodel.jspek.api.JavaSpecRunner
import info.kfgodel.jspek.api.KotlinSpec
import org.assertj.core.api.Assertions.assertThat
import org.junit.runner.RunWith

/**
 * This tests verify correct implementation of text book stack
 * Created by kfgodel on 04/07/17.
 */
@RunWith(JavaSpecRunner::class)
class TBStackTest : KotlinSpec() {
  override fun define() {
    describe("a stack") {
      val stack by let { TBStack() }

      it("is empty before any element is pushed") {
        assertThat(stack().isEmpty()).isTrue()
      }

      it("is not empty after the first element is pushed") {
        stack().push(1)
        assertThat(stack().isEmpty()).isFalse()
      }

      itThrows(IllegalStateException::class.java, "when empty and an element is popped",{
        stack().pop()
      }, {e->
        assertThat(e).hasMessage("underflow")
      })

      it("is empty after popping last element") {
        stack().push(1)
        stack().pop()
        assertThat(stack().isEmpty()).isTrue()
      }

      it("removes the top element from the stack when popped and not empty") {
        stack().push(3)
        stack().push(2)
        stack().push(1)
        assertThat(stack().pop()).isEqualTo(1)
      }

    }
  }
}
