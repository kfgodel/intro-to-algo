package info.kfgodel.intro.ch10

import info.kfgodel.jspek.api.JavaSpecRunner
import info.kfgodel.jspek.api.KotlinSpec
import org.assertj.core.api.Assertions.assertThat
import org.junit.runner.RunWith

/**
 * This tests verify correct implementation of text book stack
 * Created by kfgodel on 04/07/17.
 */
@RunWith(JavaSpecRunner::class)
class TBLinkedListTest : KotlinSpec() {
  override fun define() {
    describe("a linked list") {
      val list by let { TBLinkedList() }

      it("returns nil when empty and element searched") {
        assertThat(list().search(1)).isEqualTo(TBLinkedList.NIL)
      }

      it("return an existing element when searched by key") {
        list().insert(TBSingleLink(1))
        val found = list().search(1)
        assertThat(found.key).isEqualTo(1)
      }

      it("returns nil after an element has been deleted") {
        list().insert(TBSingleLink(1))
        val found = list().search(1)

        list().delete(found)

        assertThat(list().search(1)).isEqualTo(TBLinkedList.NIL)
      }

    }
  }
}
