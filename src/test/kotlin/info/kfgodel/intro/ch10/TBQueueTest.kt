package info.kfgodel.intro.ch10

import info.kfgodel.jspek.api.JavaSpecRunner
import info.kfgodel.jspek.api.KotlinSpec
import org.assertj.core.api.Assertions.assertThat
import org.junit.runner.RunWith

/**
 * This tests verify correct implementation of text book queue
 * Created by kfgodel on 04/07/17.
 */
@RunWith(JavaSpecRunner::class)
class TBQueueTest : KotlinSpec() {
  override fun define() {
    describe("a queue") {
      val queue by let { TBQueue() }

      itThrows(IllegalStateException::class.java, "when empty and an element is dequeued",{
        queue().deque()
      }, {e->
        assertThat(e).hasMessage("underflow")
      })

      it("dequeues an element after being queued") {
        queue().queue(1)
        assertThat(queue().deque()).isEqualTo(1)
      }

      it("dequeues the first element being queued") {
        queue().queue(3)
        queue().queue(2)
        queue().queue(1)
        assertThat(queue().deque()).isEqualTo(3)
      }

    }
  }
}
