package info.kfgodel.intro.ch06

import info.kfgodel.jspek.api.JavaSpecRunner
import info.kfgodel.jspek.api.KotlinSpec
import org.assertj.core.api.Assertions.assertThat
import org.junit.runner.RunWith

/**
 * Verifies that priority queue operations are correctly implemented
 * Created by kfgodel on 14/02/22.
 */
@RunWith(JavaSpecRunner::class)
class TBPriorityQueueTest : KotlinSpec() {
  override fun define() {
    describe("a priority queue") {
      val queue by let { TBPriorityQueue() }

      itThrows(IllegalStateException::class.java, "when empty and extract max called",{
        queue().extractMax()
      }) { e ->
        assertThat(e).hasMessage("heap underflow")
      }

      it("returns only element when extracted after first insertion") {
        queue().insert(3)
        val extracted = queue().extractMax()
        assertThat(extracted).isEqualTo(3)
      }

      it("returns decreasing order elements when 3 inserted") {
        queue().insert(3)
        queue().insert(6)
        queue().insert(1)
        assertThat(queue().extractMax()).isEqualTo(6)
        assertThat(queue().extractMax()).isEqualTo(3)
        assertThat(queue().extractMax()).isEqualTo(1)
      }

    }
  }
}

