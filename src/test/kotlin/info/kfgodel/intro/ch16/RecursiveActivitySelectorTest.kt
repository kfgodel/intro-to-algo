package info.kfgodel.intro.ch16

import info.kfgodel.jspek.api.JavaSpecRunner
import info.kfgodel.jspek.api.KotlinSpec
import org.assertj.core.api.Assertions.assertThat
import org.junit.runner.RunWith

/**
 * Date: 30/8/20 - 18:48
 */
@RunWith(JavaSpecRunner::class)
class RecursiveActivitySelectorTest : KotlinSpec() {
  val exampleS = arrayOf(1, 3, 0, 5, 3, 5, 6, 8, 8, 2, 12)
  val exampleF = arrayOf(4, 5, 6, 7, 9, 9, 10, 11, 12, 14, 16)

  override fun define() {
    describe("an activity selector") {
      val selector by let { RecursiveActivitySelector() }

      it("selects no activity if none available") {
        val selected = selector().select(arrayOf(), arrayOf(), 0, 0)
        assertThat(selected).isEmpty()
      }

      it("selects no activity if selected range includes no activity") {
        val selected = selector().select(exampleS, exampleF, 12, 11)
        assertThat(selected).isEmpty()
      }
      it("selects an optimal list for the example input") {
        val selected = selector().select(exampleS, exampleF, 0, 11)
        assertThat(selected).isEqualTo(listOf(1,4,8,11))
      }
    }
  }

}
