package info.kfgodel.intro.ch16

import info.kfgodel.jspek.api.JavaSpecRunner
import info.kfgodel.jspek.api.KotlinSpec
import org.assertj.core.api.Assertions.assertThat
import org.junit.runner.RunWith

/**
 * Date: 30/8/20 - 18:48
 */
@RunWith(JavaSpecRunner::class)
class BruteForceTaskSchedulerTest : KotlinSpec() {
  val exampleTask = arrayOf(
    Task(1, 4, 70),
    Task(2, 2, 60),
    Task(3, 4, 50),
    Task(4, 3, 40),
    Task(5, 1, 30),
    Task(6, 4, 20),
    Task(7, 6, 10)
  )

  override fun define() {
    describe("an intuitive task scheduler") {
      val scheduler by let { BruteForceTaskScheduler() }

      it("schedules no activity if input list is empty") {
        val scheduled = scheduler().schedule(arrayOf())
        assertThat(scheduled).isEmpty()
      }

      it("schedules truing every permutation and keeping the first with lowest cost") {
        val scheduled = scheduler().schedule(exampleTask)
        val taskIds = scheduled.map { task -> task.id }
        assertThat(taskIds).isEqualTo(listOf(2, 4, 1, 3, 6, 7, 5))
      }

      it("schedules with optimal cost") {
        val scheduled = scheduler().schedule(exampleTask)
        var cost = 0
        for (i in scheduled.indices) {
          val task = scheduled[i]
          if (i > task.deadline - 1) { // deadlines are 1-based
            cost += task.penalty
          }
        }
        assertThat(cost).isEqualTo(30 + 20)
      }
    }
  }

}
