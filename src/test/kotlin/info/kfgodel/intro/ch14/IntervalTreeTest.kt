package info.kfgodel.intro.ch14

import info.kfgodel.jspek.api.JavaSpecRunner
import info.kfgodel.jspek.api.KotlinSpec
import org.assertj.core.api.Assertions.assertThat
import org.junit.runner.RunWith

/**
 * Date: 6/5/20 - 00:49
 */
@RunWith(JavaSpecRunner::class)
class IntervalTreeTest : KotlinSpec() {
  override fun define() {
    describe("an interval tree") {
      val tree by let { IntervalTree() }

      it("se crea vacio") {
        assertThat(tree().size()).isEqualTo(0)
      }

      it("permite la insercion de 1 elemento") {
        tree().insert(19 to 20)
        assertThat(tree().size()).isEqualTo(1)
      }

      describe("dado un estado inicial") {
        beforeEach {
          tree()
            .insert(8 to 9)
            .insert(6 to 10)
            .insert(0 to 3)
            .insert(5 to 8)
            .insert(25 to 30)
            .insert(19 to 20)
            .insert(15 to 23)
            .insert(16 to 21)
            .insert(26 to 26)
            .insert(17 to 19)
        }

        it("permite obtener los intervalos ordenados por inicio") {
          assertThat(tree().intervals()).isEqualTo(arrayListOf(
            0 to 3,
            5 to 8,
            6 to 10,
            8 to 9,
            15 to 23,
            16 to 21,
            17 to 19,
            19 to 20,
            25 to 30,
            26 to 26
          ))
        }

        it("mantiene un arbol mas o menos balanceado") {
          assertThat(tree().leftCount()).isEqualTo(4)
          assertThat(tree().rightCount()).isEqualTo(5)
        }

        it("mantiene el balance al quitar un elemento") {
          tree().delete(15 to 23)

          assertThat(tree().leftCount()).isEqualTo(4)
          assertThat(tree().rightCount()).isEqualTo(4)
        }
      }
    }
  }
}
