package info.kfgodel.intro.ch12

import info.kfgodel.jspek.api.JavaSpecRunner
import info.kfgodel.jspek.api.KotlinSpec
import org.assertj.core.api.Assertions.assertThat
import org.junit.runner.RunWith

/**
 * This tests verify correct implementation of text book stack
 * Created by kfgodel on 04/07/17.
 */
@RunWith(JavaSpecRunner::class)
class TBBinaryTreeTest : KotlinSpec() {
  override fun define() {
    describe("a binary tree") {
      val tree by let { TBBinaryTree() }

      it("returns nil when empty and a key is searched") {
        assertThat(tree().search(1)).isEqualTo(TBBinaryTree.NIL)
      }

      it("returns nil when empty and minimum is requested") {
        assertThat(tree().minimum()).isEqualTo(TBBinaryTree.NIL)
      }
      it("returns nil when empty and maximum is requested") {
        assertThat(tree().maximum()).isEqualTo(TBBinaryTree.NIL)
      }

      it("returns inserted element when searched"){
        tree().insert(TBBinaryTreeNode(1))
        assertThat(tree().search(1).key).isEqualTo(1)
      }

      it("returns maximum and minimum of inserted elements"){
        tree().insert(TBBinaryTreeNode(2))
        tree().insert(TBBinaryTreeNode(3))
        tree().insert(TBBinaryTreeNode(1))
        assertThat(tree().maximum().key).isEqualTo(3)
        assertThat(tree().minimum().key).isEqualTo(1)
      }

      it("returns nil when searching a deleted element") {
        val node = TBBinaryTreeNode(1)
        tree().insert(node)
        tree().delete(node)
        assertThat(tree().search(1)).isEqualTo(TBBinaryTree.NIL)
      }

      it("returns nil when empty and successor is requested") {
        assertThat(tree().successor(TBBinaryTreeNode(1))).isEqualTo(TBBinaryTree.NIL)
      }
      it("returns nil when empty and maximum is requested") {
        assertThat(tree().predecessor(TBBinaryTreeNode(1))).isEqualTo(TBBinaryTree.NIL)
      }

      it("returns the successor and predecessor of an inserted element") {
        val node = TBBinaryTreeNode(5)
        tree().insert(node)
        tree().insert(TBBinaryTreeNode(1))
        tree().insert(TBBinaryTreeNode(2))
        tree().insert(TBBinaryTreeNode(8))
        tree().insert(TBBinaryTreeNode(7))
        assertThat(tree().successor(node).key).isEqualTo(7)
        assertThat(tree().predecessor(node).key).isEqualTo(2)
      }

    }
  }
}
