package info.kfgodel.intro.ch07

import info.kfgodel.intro.api.sorting.IntArraySorter
import info.kfgodel.intro.utils.swap

/**
 * Implementation of Q sort changing some terminology to make the algorithm
 * more understandable
 * Date: 31/1/22 - 14:57
 */
class RandomizedQSortIntArraySorter: IntArraySorter {
  override fun sort(input: IntArray): IntArray {
    quicksort(input, 0, input.size - 1)
    return input
  }

  private fun quicksort(array: IntArray, startingIndex: Int, endingIndex: Int) {
    if (startingIndex >= endingIndex) {
      return
    }
    val indexOfPivot = moveSmallerToLeftOfRandomPivot(startingIndex, endingIndex, array)
    quicksort(array, startingIndex, indexOfPivot - 1)
    quicksort(array, indexOfPivot + 1, endingIndex)
  }

  private fun moveSmallerToLeftOfRandomPivot(
      startingIndex: Int,
      endingIndex: Int,
      array: IntArray
  ): Int {
    val chosenPivot = (startingIndex..endingIndex).random()
    array.swap(chosenPivot, endingIndex)
    val newIndexOfLastElement = moveSmallerToLeftOfLastElement(array, startingIndex, endingIndex)
    return newIndexOfLastElement
  }

  private fun moveSmallerToLeftOfLastElement(array: IntArray, startingIndex: Int, endingIndex: Int): Int {
    val pivotElement = array[endingIndex]
    var newPivotIndex = startingIndex
    for (currentIndex in startingIndex until endingIndex) {
      if(array[currentIndex] <= pivotElement){
        array.swap(newPivotIndex, currentIndex)
        newPivotIndex++
      }
    }
    array.swap(newPivotIndex, endingIndex)
    return newPivotIndex
  }
}
