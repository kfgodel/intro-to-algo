package info.kfgodel.intro.ch07

import info.kfgodel.intro.api.sorting.IntArraySorter

/**
 * Implementacion del algoritmo de Q-sort segun el libreo
 * Comentario de los autores: Buenos para arrays grandes
 * Date: 31/1/22 - 14:57
 */
class TBQSortIntArraySorter: IntArraySorter {
  override fun sort(input: IntArray): IntArray {
    quicksort(input, 0, input.size - 1)
    return input
  }

  private fun quicksort(input: IntArray, p: Int, r: Int) {
    if(p < r){
      val q = partition(input,p,r)
      quicksort(input, p, q-1)
      quicksort(input,q+1,r)
    }
  }

  private fun partition(input: IntArray, p: Int, r: Int): Int {
    val x = input[r]
    var i = p-1
    for (j in p until r) {
      if(input[j] <= x){
        i += 1
        input[i] = input[j].also { input[j] = input[i] }
      }
    }
    val q = i+1
    input[q] = input[r].also { input[r] = input[q] }
    return q
  }
}
