package info.kfgodel.intro.ch14

import info.kfgodel.intro.ch13.RBColor
import kotlin.math.max

/**
 * Date: 17/8/20 - 16:39
 */
class IntervalNode {

  var key: Int
    get() = low
    set(value) {
      this.low = value
    }
  var low: Int = Integer.MIN_VALUE
  var high: Int = Integer.MAX_VALUE
  var color: RBColor? = null
  var parent: IntervalNode = this // Only true for nil or empty root
  var right: IntervalNode = this
  var left: IntervalNode = this
  val max: Int
    get() = max(high, max(left.max, right.max))
  val grandParent: IntervalNode
    get() = this.parent.parent
  val leftUncle: IntervalNode
    get() = grandParent.left
  val rightUncle: IntervalNode
    get() = grandParent.right

  override fun toString(): String {
    if(parent === this){
      return "$color[$low, $high] <- self"
    }else{
      return "$color[$low, $high] <- $parent"
    }
  }

  fun childrenCount(): Int {
    if(parent === this){
      return 0
    }
    return 1 + left.childrenCount() + right.childrenCount()
  }

  fun overlaps(other: Pair<Int, Int>): Boolean {
    return this.low <= other.second && this.high >= other.first
  }

  fun isLeftChild(): Boolean {
    return this === parent.left
  }

  fun isRightChild(): Boolean {
    return this === parent.right
  }

  fun leftChildren() : List<IntervalNode>{
    return if(left === this){
      emptyList()
    }else{
      left.asList()
    }
  }
  fun rightChildren() : List<IntervalNode>{
    return if(right === this){
      emptyList()
    }else{
      right.asList()
    }
  }

  fun asList(): List<IntervalNode> {
    if(this === parent){
      return emptyList() // Case for nil
    }
    val nodes = mutableListOf<IntervalNode>()
    nodes.addAll(this.leftChildren())
    nodes.add(this)
    nodes.addAll(this.rightChildren())
    return nodes
  }

}
