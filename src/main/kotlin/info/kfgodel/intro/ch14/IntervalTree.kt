package info.kfgodel.intro.ch14

import info.kfgodel.intro.ch13.RBColor

/**
 * This type represents the interval tree defined in chapter 14.3
 * Date: 17/8/20 - 16:38
 */
class IntervalTree {

  private val nil: IntervalNode = IntervalNode()
  private var root = nil

  fun size(): Int {
    return root.childrenCount()
  }

  fun insert(interval: Pair<Int, Int>): IntervalTree {
    val newNode = IntervalNode()
    newNode.low = interval.first
    newNode.high = interval.second
    val parent = findParentFor(newNode)
    newNode.parent = parent
    if (parent === nil) {
      root = newNode
    } else {
      if (newNode.key < parent.key) {
        parent.left = newNode
      } else {
        parent.right = newNode
      }
    }
    newNode.left = nil
    newNode.right = nil
    newNode.color = RBColor.RED
    insertFixup(newNode)
    return this
  }

  private fun findParentFor(newNode: IntervalNode): IntervalNode {
    var parent = nil
    var cursor = root
    while (cursor !== nil) {
      parent = cursor
      if (newNode.key < cursor.key) {
        cursor = cursor.left
      } else {
        cursor = cursor.right
      }
    }
    return parent
  }

  private fun insertFixup(insertedNode: IntervalNode) {
    var fixable = insertedNode
    var uncle: IntervalNode
    // Nil checks are not in original algorithm but are necessary to prevent wrong coloring
    while (fixable.parent !== nil && fixable.grandParent !== nil && fixable.parent.color == RBColor.RED) {
      if (fixable.parent.isLeftChild()) {
        uncle = fixable.rightUncle
        if (uncle.color == RBColor.RED) {
          uncle.color = RBColor.BLACK
          fixable.parent.color = RBColor.BLACK
          fixable.grandParent.color = RBColor.RED
          fixable = fixable.grandParent
        } else {
          if (fixable.isRightChild()) {
            fixable = fixable.parent
            leftRotate(fixable)
          }
          fixable.parent.color = RBColor.BLACK
          fixable.grandParent.color = RBColor.RED
          rightRotate(fixable.grandParent)
        }
      } else {
        uncle = fixable.leftUncle
        if (uncle.color == RBColor.RED) {
          uncle.color = RBColor.BLACK
          fixable.parent.color = RBColor.BLACK
          fixable.grandParent.color = RBColor.RED
          fixable = fixable.grandParent
        } else {
          if (fixable.isLeftChild()) {
            fixable = fixable.parent
            rightRotate(fixable)
          }
          fixable.parent.color = RBColor.BLACK
          fixable.grandParent.color = RBColor.RED
          leftRotate(fixable.grandParent)
        }
      }
    }
    root.color = RBColor.BLACK
  }

  private fun rightRotate(rotatedNode: IntervalNode) {
    val leftChild = rotatedNode.left
    rotatedNode.left = leftChild.right
    if(leftChild.right !== nil){
      leftChild.right.parent = rotatedNode
    }
    leftChild.parent = rotatedNode.parent
    if (rotatedNode.parent === nil) {
      root = leftChild
    } else {
      if (rotatedNode.isRightChild()) {
        rotatedNode.parent.right = leftChild
      } else {
        rotatedNode.parent.left = leftChild
      }
    }
    leftChild.right = rotatedNode
    rotatedNode.parent = leftChild
  }

  private fun leftRotate(rotatedNode: IntervalNode) {
    val rightChild = rotatedNode.right
    rotatedNode.right = rightChild.left
    if (rightChild.left !== nil) {
      rightChild.left.parent = rotatedNode
    }
    rightChild.parent = rotatedNode.parent
    if (rotatedNode.parent === nil) {
      root = rightChild
    } else if (rotatedNode === rotatedNode.parent.left) {
      rotatedNode.parent.left = rightChild
    } else {
      rotatedNode.parent.right = rightChild
    }
    rightChild.left = rotatedNode
    rotatedNode.parent = rightChild
  }

  fun delete(interval: Pair<Int, Int>): IntervalTree {
    val z = intervalSearch(interval)
    if (z === nil) {
      return this// there's no such key on this tree
    }
    var y = z
    var yOriginalColor = y.color
    val x: IntervalNode
    if (z.left === nil) {
      x = z.right
      transplant(z, z.right)
    } else if (z.right === nil) {
      x = z.left
      transplant(z, z.left)
    } else {
      y = minimum(z.right)
      yOriginalColor = y.color
      x = y.right
      if (y.parent === z) {
        if (x !== nil) { // This check is not on the book, but prevents assigning a parent to nil
          x.parent = y
        }
      } else {
        transplant(y, y.right)
        y.right = z.right
        y.right.parent = y
      }
      transplant(z, y)
      y.left = z.left
      y.left.parent = y
      y.color = z.color
    }
    if (yOriginalColor == RBColor.BLACK) {
      deleteFixup(x)
    }
    return this
  }

  private fun intervalSearch(interval: Pair<Int, Int>): IntervalNode {
    var x = root
    while (x !== nil && !x.overlaps(interval)) {
      if (x.left != nil && x.left.max >= interval.first) {
        x = x.left
      } else {
        x = x.right
      }
    }
    return x
  }

  private fun deleteFixup(deletedNode: IntervalNode) {
    var x = deletedNode
    while (x !== root && x.color == RBColor.BLACK) {
      if (x === x.parent.left) {
        var w = x.parent.right
        if (w.color == RBColor.RED) {
          w.color = RBColor.BLACK
          x.parent.color = RBColor.RED
          leftRotate(x.parent)
          w = x.parent.right
        }
        if (w.left.color == RBColor.BLACK && w.right.color == RBColor.BLACK) {
          w.color = RBColor.RED
          x = x.parent
        } else {
          if (w.right.color == RBColor.BLACK) {
            w.left.color = RBColor.BLACK
            w.color = RBColor.RED
            rightRotate(w)
            w = x.parent.right
          }
          w.color = x.parent.color
          x.parent.color = RBColor.BLACK
          w.right.color = RBColor.BLACK
          leftRotate(x.parent)
          x = root
        }
      } else {
        var w = x.parent.left
        if (w.color == RBColor.RED) {
          w.color = RBColor.BLACK
          x.parent.color = RBColor.RED
          rightRotate(x.parent)
          w = x.parent.left
        }
        if (w.right.color == RBColor.BLACK && w.left.color == RBColor.BLACK) {
          w.color = RBColor.RED
          x = x.parent
        } else {
          if (w.left.color == RBColor.BLACK) {
            w.right.color = RBColor.BLACK
            w.color = RBColor.RED
            leftRotate(w)
            w = x.parent.left
          }
          w.color = x.parent.color
          x.parent.color = RBColor.BLACK
          w.left.color = RBColor.BLACK
          rightRotate(x.parent)
          x = root
        }
      }
    }
    x.color = RBColor.BLACK
  }

  private fun minimum(node: IntervalNode): IntervalNode {
    var currentNode = node
    while (currentNode !== nil) {
      if (currentNode.left === nil) {
        break
      }
      currentNode = currentNode.left
    }
    return currentNode
  }

  private fun transplant(u: IntervalNode, v: IntervalNode) {
    if (u.parent === nil) {
      root = v
    } else if (u === u.parent.left) {
      u.parent.left = v
    } else {
      u.parent.right = v
    }
    if (v !== nil) { // Not on the book but prevent assigning a parent to nil
      v.parent = u.parent
    }
  }

  fun leftCount(): Int {
    return if (root == nil) 0 else root.left.childrenCount()
  }

  fun rightCount(): Int {
    return if (root == nil) 0 else root.right.childrenCount()
  }

  fun intervals(): List<Pair<Int, Int>> {
    return root.asList()
      .map { node -> node.low to node.high }
  }

}
