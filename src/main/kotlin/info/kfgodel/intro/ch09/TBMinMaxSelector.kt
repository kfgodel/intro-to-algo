package info.kfgodel.intro.ch09

/**
 * This class implements the algorithm described in pg.215 for selecting the min and max numbers from an array
 * Date: 16/2/22 - 01:19
 */
class TBMinMaxSelector {
  fun select(input: IntArray): Pair<Int, Int> {
    if(input.isEmpty()){
      throw IllegalArgumentException("at least one element is needed")
    }
    var min = input[0]
    var max = input[0]
    for (i in 1 until input.size step 2){
      var smaller = input[i]
      var bigger = if(i+1 < input.size) input[i+1] else smaller
      if(smaller > bigger){
        bigger = smaller.also { smaller = bigger }
      }
      if(smaller < min){
        min = smaller
      }
      if(bigger > max){
        max = bigger
      }
    }
    return Pair(min, max)
  }
}
