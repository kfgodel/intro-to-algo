package info.kfgodel.intro.ch08

import info.kfgodel.intro.api.sorting.IntArraySorter

/**
 * Text book implementation of counting sort from pg. 195
 * Date: 16/02/22 - 00:13
 */
class TBCountingSortIntArraySorter : IntArraySorter {
  override fun sort(input: IntArray): IntArray {
    return countingSort(input)
  }

  private fun countingSort(a: IntArray): IntArray {
    val k = 10 // assume input numbers are in the range [0-9]
    val c = IntArray(k)
    for (i in 0 until k) { // Not really needed but included to follow text book
      c[i] = 0
    }
    for (j in 0 until a.size) {
      c[a[j]] = c[a[j]] + 1
    }
    for (i in 1 until k){
      c[i] = c[i] + c[i-1]
    }
    val b = IntArray(a.size)
    for (j in a.size - 1 downTo 0){
      b[c[a[j]] - 1] = a[j]
      c[a[j]] = c[a[j]] - 1
    }
    return b
  }
}
