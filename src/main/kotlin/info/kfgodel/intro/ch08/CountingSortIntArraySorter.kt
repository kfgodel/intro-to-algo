package info.kfgodel.intro.ch08

import info.kfgodel.intro.api.sorting.IntArraySorter

/**
 * Text book implementation of counting sort from pg. 195
 * Date: 16/02/22 - 00:13
 */
class CountingSortIntArraySorter : IntArraySorter {
  override fun sort(input: IntArray): IntArray {
    return countingSort(input)
  }

  private fun countingSort(input: IntArray): IntArray {
    val counters = countOccurrencesOfPreviousNumbers(input)
    val sorted = copyUsingPreviousCountAsIndex(input, counters)
    return sorted
  }

  private fun copyUsingPreviousCountAsIndex(input: IntArray, counters: IntArray): IntArray {
    val sorted = IntArray(input.size)
    for (lastIndex in input.size - 1 downTo 0) {
      val lastNumber = input[lastIndex]
      val previousNumbersCount = counters[lastNumber]
      sorted[previousNumbersCount - 1] = lastNumber
      counters[lastNumber]-- // In case of duplicates
    }
    return sorted
  }

  private fun countOccurrencesOfPreviousNumbers(input: IntArray): IntArray {
    val highestNumber = 10 // assume input numbers are in the range [0-9]
    val counters = IntArray(highestNumber)
    countOccurrencesOfEachNumber(counters, input)
    sumPreviousCountToeachCount(counters)
    return counters
  }

  private fun sumPreviousCountToeachCount(counters: IntArray) {
    for (i in 1 until counters.size) {
      counters[i] += counters[i - 1]
    }
  }

  private fun countOccurrencesOfEachNumber(counters: IntArray, input: IntArray): IntArray {
    for (i in 0 until input.size) {
      val currentNumber = input[i]
      counters[currentNumber]++
    }
    return counters
  }
}
