package info.kfgodel.intro.utils

import java.util.WeakHashMap

/**
 * Date: 14/2/22 - 22:29
 */
fun IntArray.swap(i: Int, j: Int) {
  this[i] = this[j].also { this[j] = this[i] }
}

val heapSizePropOnIntArrayBackingMap = WeakHashMap<IntArray, Int>()
var IntArray.heapSize: Int
  get() = heapSizePropOnIntArrayBackingMap.computeIfAbsent(this) { 0 }
  set(value) {
    heapSizePropOnIntArrayBackingMap[this] = value
  }

val topPropOnIntArrayBackingMap = WeakHashMap<IntArray, Int>()
var IntArray.top: Int
  get() = topPropOnIntArrayBackingMap.computeIfAbsent(this) { 0 }
  set(value) {
    topPropOnIntArrayBackingMap[this] = value
  }

val tailPropOnIntArrayBackingMap = WeakHashMap<IntArray, Int>()
var IntArray.tail: Int
  get() = tailPropOnIntArrayBackingMap.computeIfAbsent(this) { 0 }
  set(value) {
    tailPropOnIntArrayBackingMap[this] = value
  }

val headPropOnIntArrayBackingMap = WeakHashMap<IntArray, Int>()
var IntArray.head: Int
  get() = headPropOnIntArrayBackingMap.computeIfAbsent(this) { 0 }
  set(value) {
    headPropOnIntArrayBackingMap[this] = value
  }
