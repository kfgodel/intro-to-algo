package info.kfgodel.intro.api.functions

/**
 * Takes two integers and evaluates a predicate in them
 * Date: 23/12/17 - 12:45
 */
interface BiIntPredicate {
    /**
     * Indicates true if the given Ints match the condition represented by this instance
     * @param a first input
     * @param b second input
     * @return The result
     */
    fun test(a: Int, b: Int): Boolean
}
