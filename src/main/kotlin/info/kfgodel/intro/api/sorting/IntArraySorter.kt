package info.kfgodel.intro.api.sorting

/**
 * Este tipo representa un ordenador de elementos en un array de ints
 * Created by kfgodel on 04/07/17.
 */
interface IntArraySorter : Sorter<IntArray>
