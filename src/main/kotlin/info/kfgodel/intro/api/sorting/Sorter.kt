package info.kfgodel.intro.api.sorting

/**
 * Este tipo representa un ordenador de elementos
 * Created by kfgodel on 04/07/17.
 */
interface Sorter<T> {
    /**
     * Ordena los elementos del input y devuelve el resutlado.<br></br>
     * Dependiendo del algoritmo el resultado puede ser el mismo input o no
     * @param input El conjunto de elementos a ordenar
     * @return El conjunto ordenado
     */
    fun sort(input: T): T
}
