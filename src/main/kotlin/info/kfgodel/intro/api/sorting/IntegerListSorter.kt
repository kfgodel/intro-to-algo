package info.kfgodel.intro.api.sorting

/**
 * Este tipo representa un sorter pero de lista de elementos
 * Created by kfgodel on 04/07/17.
 */
interface IntegerListSorter : Sorter<List<Int>>
