package info.kfgodel.intro.ch15

import java.lang.IllegalStateException
import java.lang.StringBuilder

/**
 * This type represents the Longest Common Sequence finder. As defined in chapter 15
 * Date: 30/8/20 - 18:47
 */
class LcsFinder {
  private val UP_LEFT = -3
  private val UP = -1
  private val LEFT = -2

  fun findBetween(x: String, y: String): String {
    val (c, b) = lcsLength(x, y)
    val xIndices = findXIndicesWith(c,b)
    return reconstructFrom(x,xIndices)
  }

  private fun reconstructFrom(x: String, xIndices: Array<Int?>): String {
    val lcs = StringBuilder(xIndices.size)
    for(element in xIndices){
      val xIndex = element!!
      val char = x[xIndex]
      lcs.append(char)
    }
    return lcs.toString()
  }

  private fun findXIndicesWith(c: Array<Array<Int?>>, b: Array<Array<Int?>>): Array<Int?> {
    var i = c.size - 1
    var j = c[0].size - 1
    val lcsLength = c[i][j]!!
    val xIndices = arrayOfNulls<Int>(lcsLength)
    while(c[i][j]!! > 0){ // still more chars to find
      val direction = b[i][j]
      when(direction){
        UP_LEFT ->{
          val subsquenceIndex = c[i][j]!! - 1
          xIndices[subsquenceIndex] = i - 1 // Strings are 0-based
          i -= 1
          j -= 1
        }
        UP->{
          i -= 1
        }
        LEFT -> {
          j -= 1
        }
        else -> {
          throw IllegalStateException("Direction should be one of the 3 possible")
        }
      }
    }
    return xIndices
  }

  private fun lcsLength(x: String, y: String): Pair<Array<Array<Int?>>, Array<Array<Int?>>> {
    val m = x.length
    val n = y.length
    val b = Array<Array<Int?>>(m + 1) { arrayOfNulls(n + 1) } // Leave 0 unused for b to match c positions
    val c = Array<Array<Int?>>(m + 1) { arrayOfNulls(n + 1) }
    for (i in 1..m) {
      c[i][0] = 0
    }
    for (j in 0..n) {
      c[0][j] = 0
    }
    for (i in 1..m) {
      for (j in 1..n) {
        when {
            x[i-1] == y[j-1] -> { // String indices are 0-based
              c[i][j] = c[i - 1][j - 1]!! + 1
              b[i][j] = UP_LEFT
            }
            c[i - 1][j]!! >= c[i][j - 1]!! -> {
              c[i][j] = c[i - 1][j]
              b[i][j] = UP
            }
            else -> {
              c[i][j] = c[i][j - 1]
              b[i][j] = LEFT
            }
        }
      }
    }
    return c to b
  }

}
