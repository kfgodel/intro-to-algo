package info.kfgodel.intro.ch15

/**
 * This type represents a rod cutter from the examples of dynamic programming
 * Date: 19/8/20 - 20:58
 */
class RodCutter {

  fun cut(rodLength: Int): CuttingPlan {
    return cutRod(rodLength)
  }

  private fun cutRod(rodLength: Int): CuttingPlan {
    var bestPlan = CuttingPlan(0, arrayOf())
    if(rodLength == 0){
      return bestPlan
    }
    for (i in 1..rodLength) {
      val piecePlan = CuttingPlan(priceOf(i), arrayOf(i))
      val remainderPlan = cutRod(rodLength - i)
      val combined = piecePlan.combinedWith(remainderPlan)
      if(combined.revenue > bestPlan.revenue) {
        bestPlan = combined
      }
    }
    return bestPlan
  }

  private fun priceOf(pieceLength: Int): Int {
    return when (pieceLength) {
      1 -> 1
      2 -> 5
      3 -> 8
      4 -> 9
      5 -> 10
      6 -> 17
      7 -> 17
      8 -> 20
      9 -> 24
      10 -> 30
      else -> {
        Integer.MIN_VALUE
      }
    }
  }
}
