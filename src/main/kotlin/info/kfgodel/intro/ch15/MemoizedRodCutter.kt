package info.kfgodel.intro.ch15

/**
 * This type represents a rod cutter implemented with memoized top-down algorithm
 * Date: 19/8/20 - 20:58
 */
class MemoizedRodCutter {

  fun cut(rodLength: Int): CuttingPlan {
    val memo: Array<CuttingPlan?> = arrayOfNulls(rodLength + 1 )
    return memoizedCutRod(rodLength, memo)
  }

  private fun memoizedCutRod(rodLength: Int, memo: Array<CuttingPlan?>): CuttingPlan {
    if(memo[rodLength] !== null){
      return memo[rodLength] ?: error("It should be non null")
    }
    var bestPlan = CuttingPlan(0, arrayOf())
    if(rodLength == 0){
      return bestPlan
    }
    for (i in 1..rodLength) {
      val piecePlan = CuttingPlan(priceOf(i), arrayOf(i))
      val remainderPlan = memoizedCutRod(rodLength - i, memo)
      val combined = piecePlan.combinedWith(remainderPlan)
      if(combined.revenue > bestPlan.revenue) {
        bestPlan = combined
      }
    }
    memo[rodLength] = bestPlan
    return bestPlan
  }

  private fun priceOf(pieceLength: Int): Int {
    return when (pieceLength) {
      1 -> 1
      2 -> 5
      3 -> 8
      4 -> 9
      5 -> 10
      6 -> 17
      7 -> 17
      8 -> 20
      9 -> 24
      10 -> 30
      else -> {
        Integer.MIN_VALUE
      }
    }
  }
}
