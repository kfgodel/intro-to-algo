package info.kfgodel.intro.ch15

/**
 * This type represents a rod cutter from the examples of dynamic programming
 * Date: 19/8/20 - 20:58
 */
class BottomUpRodCutter {

  fun cut(rodLength: Int): CuttingPlan {
    val memo: Array<CuttingPlan?> = arrayOfNulls(rodLength + 1 )
    memo[0] = CuttingPlan(0, arrayOf())
    return bottomUpCutRod(rodLength, memo)
  }

  private fun bottomUpCutRod(rodLength: Int, memo: Array<CuttingPlan?>): CuttingPlan {
    for(j in 1..rodLength){
      var bestPlan = memo[0]!!
      for (i in 1..j) {
        val piecePlan = CuttingPlan(priceOf(i), arrayOf(i))
        val remainderPlan = memo[j - i]!!
        val combined = piecePlan.combinedWith(remainderPlan)
        if(combined.revenue > bestPlan.revenue) {
          bestPlan = combined
        }
      }
      memo[j] = bestPlan
    }
    return memo[rodLength]!!
  }

  private fun priceOf(pieceLength: Int): Int {
    return when (pieceLength) {
      1 -> 1
      2 -> 5
      3 -> 8
      4 -> 9
      5 -> 10
      6 -> 17
      7 -> 17
      8 -> 20
      9 -> 24
      10 -> 30
      else -> {
        Integer.MIN_VALUE
      }
    }
  }
}
