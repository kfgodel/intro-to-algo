package info.kfgodel.intro.ch15

class CuttingPlan(val revenue: Int, val parts: Array<Int>) {
  fun combinedWith(other: CuttingPlan): CuttingPlan {
    return CuttingPlan(this.revenue + other.revenue, this.parts + other.parts)
  }

}
