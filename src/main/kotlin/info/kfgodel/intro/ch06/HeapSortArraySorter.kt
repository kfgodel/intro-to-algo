package info.kfgodel.intro.ch06

import info.kfgodel.intro.api.sorting.IntArraySorter
import info.kfgodel.intro.utils.heapSize
import info.kfgodel.intro.utils.swap

/**
 * Implementation of heap sort that uses a different naming and structure so the altgorithm
 * is easier to understand reading the code
 * Date: 6/2/22 - 11:29
 */
class HeapSortArraySorter : IntArraySorter {
  override fun sort(array: IntArray): IntArray {
    buildImplicitHeapTreeOn(array)
    for (endOfHeapIndex in array.size - 1 downTo 1){
      array.swap(0, endOfHeapIndex)
      array.heapSize -= 1
      swapWithLargestChild(0, array)
    }
    return array
  }

  private fun buildImplicitHeapTreeOn(array: IntArray) {
    array.heapSize = array.size
    val lastParentIndex = array.size / 2
    for (currentIndex in lastParentIndex downTo 0){ //0 is the root
      swapWithLargestChild(currentIndex, array)
    }
  }

}

fun swapWithLargestChild(parentIndex: Int, array: IntArray) {
  val leftIndex = leftChildOf(parentIndex)
  val rightIndex = rightChildOf(parentIndex)
  var largestIndex = if(leftIndex < array.heapSize && array[leftIndex] > array[parentIndex]) {
    leftIndex
  } else {
    parentIndex
  }
  if(rightIndex < array.heapSize && array[rightIndex] > array[largestIndex]){
    largestIndex = rightIndex
  }
  if(largestIndex != parentIndex){
    array.swap(parentIndex, largestIndex)
    swapWithLargestChild(largestIndex, array)
  }
}

private fun leftChildOf(i: Int): Int = 2*(i+1) - 1
private fun rightChildOf(i: Int): Int = 2*(i+1)
