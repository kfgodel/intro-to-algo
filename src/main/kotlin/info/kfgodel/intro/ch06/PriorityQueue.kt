package info.kfgodel.intro.ch06

import info.kfgodel.intro.utils.heapSize
import info.kfgodel.intro.utils.swap

/**
 * Implementation changing names and structure to improve understanding of the algorithm
 * Date: 15/2/22 - 21:11
 */
class PriorityQueue {
  val internalHeap = IntArray(10)

  fun extractMax(): Int {
    if (internalHeap.heapSize < 1) {
      throw IllegalStateException("heap underflow")
    }
    val max = removeFirst()
    return max
  }

  private fun removeFirst(): Int {
    val firstElement = internalHeap[0]
    internalHeap[0] = internalHeap[internalHeap.heapSize - 1].also { internalHeap.heapSize-- }
    swapWithLargestChild(0, internalHeap)
    return firstElement
  }

  fun insert(key: Int) {
    val insertedSpace = addSpace()
    increaseKey(insertedSpace, key)
  }

  private fun addSpace(): Int {
    val newSpaceIndex = internalHeap.heapSize
    internalHeap[newSpaceIndex] = Int.MIN_VALUE
    internalHeap.heapSize += 1
    return newSpaceIndex
  }

  private fun increaseKey(increasedIndex: Int, key: Int) {
    if (key < internalHeap[increasedIndex]) {
      // Is this needed?
      throw IllegalArgumentException("new key is smaller than current key")
    }
    internalHeap[increasedIndex] = key
    swapParentsIfUnordered(increasedIndex)
  }

  private fun swapParentsIfUnordered(startingIndex: Int) {
    var childIndex = startingIndex
    while (childIndex > 0) {
      val parentIndex = parent(childIndex)
      if (internalHeap[parentIndex] >= internalHeap[childIndex]) {
        break // the rest is ordered
      }
      internalHeap.swap(childIndex, parentIndex)
      childIndex = parentIndex
    }
  }

  private fun parent(i: Int): Int = i / 2
}
