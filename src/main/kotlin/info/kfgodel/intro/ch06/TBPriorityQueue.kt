package info.kfgodel.intro.ch06

import info.kfgodel.intro.utils.heapSize
import info.kfgodel.intro.utils.swap

/**
 * Text book implementation using same nomenclature
 * Date: 15/2/22 - 21:11
 */
class TBPriorityQueue {
  val a = IntArray(10)

  fun extractMax(): Int {
    if(a.heapSize < 1){
      throw IllegalStateException("heap underflow")
    }
    val max = a[0]
    a[0] = a[a.heapSize-1]
    a.heapSize -= 1
    maxHeapify(a, 0)
    return max
  }

  fun insert(key: Int) {
    a.heapSize += 1
    val insertIndex = a.heapSize - 1
    a[insertIndex] = Int.MIN_VALUE
    increaseKey(insertIndex, key)
  }

  private fun increaseKey(index: Int, key: Int) {
    if(key < a[index]){
      // Is this needed?
      throw IllegalArgumentException("new key is smaller than current key")
    }
    a[index] = key
    var i = index
    while (i > 0 && a[parent(i)] < a[i]) {
      a.swap(i, parent(i))
      i = parent(i)
    }
  }

  private fun parent(i: Int): Int = i / 2
}
