package info.kfgodel.intro.ch06

import info.kfgodel.intro.api.sorting.IntArraySorter
import info.kfgodel.intro.utils.heapSize
import info.kfgodel.intro.utils.swap

/**
 * Implementacion con misma nomenclatura que el libro del heap sort para ordenar arrays de ints
 * Comentarios de autores: Ordena los elementos en el lugar, en la practica qsort le gana
 * Date: 6/2/22 - 11:29
 */
class TBHeapSortArraySorter : IntArraySorter {
  override fun sort(input: IntArray): IntArray {
    buildMaxHeap(input)
    for (i in input.size-1 downTo 1){
      input.swap(0,i)
      input.heapSize -= 1
      maxHeapify(input, 0)
    }
    return input
  }

  private fun buildMaxHeap(a: IntArray) {
    a.heapSize = a.size
    val endingIndex = a.size / 2
    for (i in endingIndex downTo 0){
      maxHeapify(a, i)
    }
  }

}

fun maxHeapify(a: IntArray, i: Int) {
  val l = left(i)
  val r = right(i)
  var largest = if(l < a.heapSize && a[l] > a[i]) l else i
  if(r < a.heapSize && a[r] > a[largest]){
    largest = r
  }
  if(largest != i){
    a.swap(i, largest)
    maxHeapify(a, largest)
  }
}

fun left(i: Int): Int = 2*(i+1) - 1
fun right(i: Int): Int = 2*(i+1)
