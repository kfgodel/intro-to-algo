package info.kfgodel.intro.ch22

import java.util.TreeMap

/**
 * This class is used to compare results of graph search returning trees
 * Date: 18/2/22 - 02:08
 */
class ComparableTree {
  val state = TreeMap<Int,MutableList<Int>>()
  fun connecting(source: Int, vararg destinations: Int): ComparableTree {
    val connections = state.computeIfAbsent(source) { ArrayList<Int>() }
    for(destination in destinations){
      connections.add(destination)
    }
    return this
  }

  override fun equals(other: Any?): Boolean {
    if (this === other) return true
    if (javaClass != other?.javaClass) return false

    other as ComparableTree

    if (state != other.state) return false

    return true
  }

  override fun hashCode(): Int {
    return state.hashCode()
  }


}
