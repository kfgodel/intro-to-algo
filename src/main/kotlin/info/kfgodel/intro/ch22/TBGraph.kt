package info.kfgodel.intro.ch22

import java.util.LinkedList

/**
 * This class implements a graph as an adjacency list as defined in chapter 22
 * Date: 18/2/22 - 01:19
 */
class TBGraph {
  val nodes = ArrayList<TBGNode>()

  /**
   * Helper to initialize graph with connected nodes
   */
  fun connecting(source: Int, vararg destinations: Int): TBGraph {
    val node = find(source)
    node.connectToAll(destinations)
    return this
  }

  private fun find(source: Int): TBGNode {
    for(node in nodes){
      if(node.key == source){
        return node
      }
    }
    val newNode = TBGNode(source)
    nodes.add(newNode)
    return newNode
  }

  fun bfs(key: Int): ComparableTree {
    for (u in nodes){
      u.color = NodeColor.WHITE
      u.d = Integer.MAX_VALUE
      u.pi = NIL
    }
    val s = find(key)
    s.color = NodeColor.GRAY
    s.d = 0
    s.pi = NIL

    val resultTree = ComparableTree()
    val q = LinkedList<TBGNode>()
    q.push(s)
    while(q.isNotEmpty()){
      val u = q.pop()
      for (adjacent in u.adjacents()){
        val v = find(adjacent.key)
        if(v.color.equals(NodeColor.WHITE)){
          v.color = NodeColor.GRAY
          v.d = u.d + 1
          q.add(v)
          resultTree.connecting(u.key, v.key)
        }
      }
      u.color = NodeColor.BLACK
    }
    return resultTree
  }

  object NIL : TBGNode(Int.MIN_VALUE) {

  }
}

enum class NodeColor {
  WHITE,
  GRAY,
  BLACK
}

open class TBGNode(val key:Int) {
  private var next: TBGNode? = null
  var color: NodeColor = NodeColor.WHITE
  var d: Int = Int.MAX_VALUE
  var pi: TBGNode = TBGraph.NIL

  fun connectToAll(destinations: IntArray) {
    var currentNode = this.lastConnection()
    for(destination in destinations){
      currentNode = currentNode.connectTo(destination)
    }
  }

  private fun lastConnection(): TBGNode {
    var currentNode = this
    while(currentNode.next != null){
      currentNode = currentNode.next!!
    }
    return currentNode
  }

  private fun connectTo(destination: Int): TBGNode {
    val newNode = TBGNode(destination)
    this.next = newNode
    return newNode
  }

  fun adjacents(): List<TBGNode> {
    val adjacents = ArrayList<TBGNode>()
    var currentNode = this.next
    while(currentNode != null){
      adjacents.add(currentNode)
      currentNode = currentNode.next
    }
    return adjacents
  }

}
