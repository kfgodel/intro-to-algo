package info.kfgodel.intro.ch16

/**
 * This type implements the task scheduler with an intuitive greedy approach (not on the book)
 * Date: 5/9/20 - 16:58
 */
class IntuitiveTaskScheduler {
  fun schedule(tasks: Array<Task>): Array<Task> {
    val unscheduled = sortByDeadline(tasks)
    val scheduled = mutableListOf<Task>()
    while (unscheduled.isNotEmpty()) {
      val earliestOrOverdue = findEarliestOrOverdueFrom(unscheduled, scheduled.size)
      val mostExpensive = findMostExpensiveOn(earliestOrOverdue)
      scheduled.add(mostExpensive)
      unscheduled.remove(mostExpensive)
    }
    return scheduled.toTypedArray()
  }

  private fun sortByDeadline(tasks: Array<Task>): MutableList<Task> {
    tasks.sortBy { task -> task.deadline }
    return tasks.toMutableList()
  }

  private fun findMostExpensiveOn(earliestOrOverdue: List<Task>): Task {
    return earliestOrOverdue.maxBy { task -> task.penalty }!! // We know there's at least 1 element
  }

  private fun findEarliestOrOverdueFrom(unscheduled: List<Task>, currentTime: Int): List<Task> {
    val earliest = mutableListOf<Task>()
    var earliestDeadline: Int? = null
    for (nextTask in unscheduled) {
      if (currentTime > nextTask.deadline - 1) { // deadline is 1-based
        earliest.add(nextTask) //It's overdue
      } else if (earliestDeadline == null) {
        // First non overdue task
        earliestDeadline = nextTask.deadline
        earliest.add(nextTask)
      } else if (earliestDeadline == nextTask.deadline) {
        earliest.add(nextTask) // Other non overdue tasks that can be considered
      } else {
        // First task after earlist deadline. Stop search
        break
      }
    }
    return earliest
  }
}
