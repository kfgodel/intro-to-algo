package info.kfgodel.intro.ch16

/**
 * This type represents the activity selector from chapter 16, implemented with an iterative algorithm
 * Date: 1/9/20 - 20:53
 */
class IterativeActivitySelector {
  fun select(s: Array<Int>, f: Array<Int>): List<Int> {
    val n = s.size
    val A = mutableListOf<Int>() // Don't add a1 yet. s may be empty
    if(n > 0){
        A.add(1)
    }
    var k = 1
    for(m in 2..n){
      if(s[m -1 ] >= f[k -1 ]){ // Array access is 0-based
        A.add(m)
        k = m
      }
    }
    return A
  }
}
