package info.kfgodel.intro.ch16

import java.lang.IllegalStateException

/**
 * This type implements the algorithm described in exercise 16-4
 * Date: 5/9/20 - 16:58
 */
class OptimalTaskScheduler {
  fun schedule(tasks: Array<Task>): Array<Task> {
    val unscheduled = sortByPenalty(tasks)
    val scheduled = arrayOfNulls<Task>(tasks.size)
    for (nextTask in unscheduled){
      var availableSlot = findOnTimeSlot(scheduled, nextTask.deadline)
      if(availableSlot < 0){
        availableSlot = findLatestSlot(scheduled)
      }
      scheduled[availableSlot] = nextTask
    }
    return scheduled as Array<Task> // Every slot is now filled
  }

  private fun findLatestSlot(scheduled: Array<Task?>): Int {
    for (i in scheduled.size-1 downTo 0){
      if(scheduled[i] === null){
        return i
      }
    }
    throw IllegalStateException("There should be an empty slot")
  }

  private fun findOnTimeSlot(scheduled: Array<Task?>, deadline: Int): Int {
    for (i in deadline-1 downTo 0){ // deadlines are 1-based
      if(scheduled[i] === null){
        return i
      }
    }
    return -1
  }

  private fun sortByPenalty(tasks: Array<Task>): Array<Task> {
    tasks.sortByDescending { task -> task.penalty }
    return tasks
  }

}
