package info.kfgodel.intro.ch16

/**
 * this type represents a single object for the data related to tasks in the scheduling problem (ch16)
 * Date: 5/9/20 - 16:44
 */
class Task(val id:Int, val deadline:Int, val penalty:Int) {

  override fun toString(): String {
    return "$id: ($deadline)->$penalty"
  }

}
