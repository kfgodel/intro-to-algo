package info.kfgodel.intro.ch16

/**
 * This type represents the activity selector from chapter 16, implemented with recursive algorithm
 * Date: 1/9/20 - 20:53
 */
class RecursiveActivitySelector {
  fun select(s: Array<Int>, f: Array<Int>, k: Int, n: Int): List<Int> {
    var m = k + 1
    while(m <= n && k > 0 && s[m-1] < f[k-1]){ // exclude k=0 acse. rest 1 for array accessing
      m++
    }
    if(m<= n){
      val result = mutableListOf(m)
      result.addAll(select(s,f,m,n))
      return result
    }else{
      return emptyList()
    }
  }
}
