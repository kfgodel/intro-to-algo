package info.kfgodel.intro.ch16

/**
 * This type implements the task scheduler with an intuitive greedy approach (not on the book)
 * Date: 5/9/20 - 16:58
 */
class BruteForceTaskScheduler {
  fun schedule(tasks: Array<Task>): Array<Task> {
    return keepCheapestPermutationFrom(0, tasks)
  }

  private fun keepCheapestPermutationFrom(startingIndex: Int, tasks: Array<Task>): Array<Task> {
    if(startingIndex >= tasks.size -1 ){ // There are not enough elements to permute
      return tasks
    }
    var cheapest = keepCheapestPermutationFrom(startingIndex+1,tasks) // Find cheapest with first element as it is
    val currentPermutation = cheapest.copyOf()
    for(i in startingIndex+1 until currentPermutation.size){
      currentPermutation.swap(startingIndex, i)
      val cheapestSubPermutation = keepCheapestPermutationFrom(startingIndex+1, currentPermutation)
      if(cheapest.cost() > cheapestSubPermutation.cost()){
        cheapest = cheapestSubPermutation
      }
    }
    return cheapest
  }

}

private fun <T> Array<T>.swap(first: Int, second: Int) {
  // Use also to avoid tmp variable
  this[first] = this[second].also { this[second] = this[first] }
}

private fun Array<Task>.cost(): Int {
  var cost = 0
  for (i in this.indices) {
    val task = this[i]
    if(task.deadline < i+1){ // deadlines are 1-based
      cost += task.penalty
    }
  }
  return cost
}
