package info.kfgodel.intro.ch02

import info.kfgodel.intro.api.sorting.IntegerListSorter
import info.kfgodel.intro.api.functions.BiIntPredicate

/**
 * Date: 23/12/17 - 12:38
 */
class InsertionSortConfigurableOrderIntegerListSorter : IntegerListSorter {
    private var orderCriterion: ((Int, Int) -> Boolean)? = null
    override fun sort(input: List<Int>): List<Int> {
      val sorted = input.toMutableList()
      reordenarDesdeElSegundo(sorted)
        return sorted
    }

    private fun reordenarDesdeElSegundo(input: MutableList<Int>) {
        for (indiceDeReordenado in 1 until input.size) {
            val elementoReordenado = input[indiceDeReordenado]
            val indiceDeAnterior = insertarAntesQueMayores(input, indiceDeReordenado, elementoReordenado)
            input[indiceDeAnterior + 1] = elementoReordenado
        }
    }

    private fun insertarAntesQueMayores(
        input: MutableList<Int>,
        indiceDeReordenado: Int,
        elementoReordenado: Int
    ): Int {
        var indiceDeAnterior = indiceDeReordenado - 1
        while (indiceDeAnterior >= 0) {
            val estabanDesordenados = cambiarLugarConAnteriorSiEsMasChico(input, indiceDeAnterior, elementoReordenado)
            if (!estabanDesordenados) {
                break
            }
            indiceDeAnterior--
        }
        return indiceDeAnterior
    }

    private fun cambiarLugarConAnteriorSiEsMasChico(
        input: MutableList<Int>,
        indiceDeAnterior: Int,
        elementoReordenado: Int
    ): Boolean {
        val elementoAnterior = input[indiceDeAnterior]
        if (orderCriterion!!.invoke(elementoAnterior, elementoReordenado)) {
            return false
        }
        input[indiceDeAnterior + 1] = elementoAnterior
        return true
    }

    companion object {
        fun create(orderCriterion: (Int, Int) -> Boolean): InsertionSortConfigurableOrderIntegerListSorter {
            val sorter = InsertionSortConfigurableOrderIntegerListSorter()
            sorter.orderCriterion = orderCriterion
            return sorter
        }
    }
}
