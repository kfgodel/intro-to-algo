package info.kfgodel.intro.ch02

import info.kfgodel.intro.api.sorting.IntArraySorter

/**
 * Implementacion con misma nomenclatura que el libro del insertion sort para ordenar arrays de ints
 * Comentarios de autores: requiere duplicar arrays, es malo para datos asociados
 * Date: 6/2/22 - 11:29
 */
class TBMergeSortArraySorter : IntArraySorter {
  override fun sort(input: IntArray): IntArray {
    mergeSort(input, 0, input.size - 1)
    return input
  }

  private fun mergeSort(input: IntArray, p: Int, r: Int) {
    if(p < r){
      val q = (p + r) / 2
      mergeSort(input, p, q)
      mergeSort(input, q + 1,r)
      merge(input, p, q, r)
    }
  }

  private fun merge(input: IntArray, p: Int, q: Int, r: Int) {
    val n1 = q -p + 1
    val n2 = r - q
    val L  = IntArray(n1 + 1)
    val R  = IntArray(n2 + 1)
    for (i in 0 until n1) {
      L[i] = input[p + i]
    }
    for (j in 0 until n2){
      R[j] = input[q +1 + j]
    }
    L[n1] = Integer.MAX_VALUE
    R[n2] = Integer.MAX_VALUE
    var i = 0
    var j = 0
    for (k in p until r+1){
      if(L[i] <= R[j]){
        input[k] = L[i]
        i += 1
      }else{
        input[k] = R[j]
        j += 1
      }
    }
  }

}
