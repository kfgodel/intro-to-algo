package info.kfgodel.intro.ch02

import info.kfgodel.intro.api.sorting.IntArraySorter

/**
 * Implementacion con misma nomenclatura que el libro del insertion sort para ordenar arrays de ints
 * Comentarios de autores: puede ser bueno para pocos valores. Tiene el peor costo de tiempo)
 * Date: 6/2/22 - 11:29
 */
class TBInsertionSortArraySorter : IntArraySorter {
  override fun sort(input: IntArray): IntArray {
    for(j in 1 until input.size){
      val key = input[j]
      var i = j-1
      while (i >= 0 && input[i] > key){
        input[i+1] = input[i]
        i -= 1
      }
      input[i+1] = key
    }
    return input
  }

}
