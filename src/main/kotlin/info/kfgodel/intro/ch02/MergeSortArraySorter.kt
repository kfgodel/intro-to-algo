package info.kfgodel.intro.ch02

import info.kfgodel.intro.api.sorting.IntArraySorter

/**
 * Implementation of merge sort that uses different terminology and structure to improve
 * understanding of the algorithm
 * Date: 6/2/22 - 11:29
 */
class MergeSortArraySorter : IntArraySorter {
  override fun sort(input: IntArray): IntArray {
    splitInHalvesSortEachAndMergeResultFrom(0, input.size - 1, input)
    return input
  }

  private fun splitInHalvesSortEachAndMergeResultFrom(startingIndex: Int, endingIndex: Int, array: IntArray) {
    if (startingIndex >= endingIndex){
      // End condition
      return
    }
    val middleIndex = (startingIndex + endingIndex) / 2
    splitInHalvesSortEachAndMergeResultFrom(startingIndex, middleIndex, array)
    splitInHalvesSortEachAndMergeResultFrom(middleIndex + 1, endingIndex, array)
    mergeSortedRangesFrom(startingIndex, middleIndex, endingIndex, array)
  }

  private fun mergeSortedRangesFrom(startOfRange1: Int, endOfRange1: Int, endOfRange2: Int, array: IntArray) {
    val leftRangeCopy = copyRangeFrom(startOfRange1, endOfRange1, array)
    val rightRangeCopy = copyRangeFrom(endOfRange1 + 1, endOfRange2, array)
    mergeSortingFrom(startOfRange1, endOfRange2, leftRangeCopy, rightRangeCopy, array)
  }

  private fun copyRangeFrom(startingIndex: Int, endingIndex: Int, array: IntArray): IntArray {
    val rangeLength = endingIndex - startingIndex + 1
    val rangeCopy = IntArray(rangeLength + 1)
    for (i in 0 until rangeLength) {
      rangeCopy[i] = array[startingIndex + i]
      }
    rangeCopy[rangeLength] = Integer.MAX_VALUE
    return rangeCopy
  }

  private fun mergeSortingFrom(
    startingIndex: Int,
    endingIndex: Int,
    leftRangeCopy: IntArray,
    rightRangeCopy: IntArray,
    array: IntArray
  ) {
    var leftIndex = 0
    var rightIndex = 0
    for (currentIndex in startingIndex until endingIndex + 1) {
      val leftElement = leftRangeCopy[leftIndex]
      val rightElement = rightRangeCopy[rightIndex]
      array[currentIndex] = if (leftElement <= rightElement) {
        leftElement.also { leftIndex++ }
      } else {
        rightElement.also { rightIndex++ }
      }
    }
  }

}
