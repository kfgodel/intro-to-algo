package info.kfgodel.intro.ch02

import info.kfgodel.intro.api.sorting.IntArraySorter

/**
 * Implementation of  insertion sort changing the names and structure to facilitate understanding
 * the algorithm
 * Created by kfgodel on 04/07/17.
 */
class InsertionSortIntArraySorter : IntArraySorter {
  override fun sort(input: IntArray): IntArray {
    sortStartingWithSecondElement(input)
    return input
  }

  private fun sortStartingWithSecondElement(array: IntArray) {
    for (currentIndex in 1 until array.size) {
      val currentElement = array[currentIndex]
      val indexOfPrevious = makeASpaceAfterPreviousStartingOn(currentIndex, currentElement, array)
      array[indexOfPrevious + 1] = currentElement
    }
  }

  private fun makeASpaceAfterPreviousStartingOn(startingIndex: Int, currentElement: Int, array: IntArray): Int {
    var indexOfPrevious = startingIndex - 1
    while (indexOfPrevious >= 0) {
      val moved = moveUpIfUnordered(indexOfPrevious, currentElement, array)
      if (!moved) {
        break
      }
      indexOfPrevious--
    }
    return indexOfPrevious
  }

  private fun moveUpIfUnordered(
    indexOfPrevious: Int,
    currentElement: Int,
    array: IntArray
  ): Boolean {
    val previousElement = array[indexOfPrevious]
    if (previousElement <= currentElement) {
      return false
    }
    array[indexOfPrevious + 1] = previousElement
    return true
  }
}
