package info.kfgodel.intro.ch10

/**
 * Implementation using nil as sentinel to generate a circular linked list.
 * This avoid checking for boundary conditions
 * Date: 16/2/22 - 13:51
 */
class TBSentinelLinkedList {
  val nil = TBSingleLink(Int.MIN_VALUE)
  private var head: TBSingleLink = nil

  fun search(k: Int): TBSingleLink {
    var x = nil.next
    while(x != nil && x.key != k){
      x = x.next
    }
    return x
  }

  fun insert(x: TBSingleLink) {
    x.next = nil.next
    nil.next.prev = x
    nil.next = x
    x.prev = nil
  }

  fun delete(x: TBSingleLink) {
    x.prev.next = x.next
    x.next.prev = x.prev
  }

}
