package info.kfgodel.intro.ch10

import info.kfgodel.intro.utils.head
import info.kfgodel.intro.utils.tail

/**
 * This class implements the text book definition of a queue as seen on pg.235
 * Date: 16/2/22 - 13:33
 */
class TBQueue {

  private val q = IntArray(10) // Assume we will at most 10 spaces

  fun queue(x: Int) {
    q[q.tail] = x
    if(q.tail == q.size - 1){
      q.tail = 0
    } else {
      q.tail++
    }
  }

  fun deque() : Int {
    if(q.head == q.tail){
      throw IllegalStateException("underflow")
    }
    val x = q[q.head]
    if(q.head == q.size - 1){
      q.head = 0
    } else {
      q.head++
    }
    return x
  }
}
