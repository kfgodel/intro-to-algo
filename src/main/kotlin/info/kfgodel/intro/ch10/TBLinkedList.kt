package info.kfgodel.intro.ch10

/**
 * Date: 16/2/22 - 13:51
 */
class TBLinkedList {
  object NIL : TBSingleLink(Int.MIN_VALUE)
  private var head: TBSingleLink = NIL

  fun search(k: Int): TBSingleLink {
    var x = head
    while(x != NIL && x.key != k){
      x = x.next
    }
    return x
  }

  fun insert(x: TBSingleLink) {
    x.next = head
    if(head != NIL){
      head.prev = x
    }
    head = x
    x.prev = NIL
  }

  fun delete(x: TBSingleLink) {
    if(x.prev != NIL){
      x.prev.next = x.next
    }else{
      head = x.next
    }
    if(x.next != NIL){
      x.next.prev = x.prev
    }
  }

}

open class TBSingleLink(val key: Int) {
  var prev: TBSingleLink = this
  var next: TBSingleLink = this
}
