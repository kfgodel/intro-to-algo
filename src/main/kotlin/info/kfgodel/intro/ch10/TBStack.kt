package info.kfgodel.intro.ch10

import info.kfgodel.intro.utils.top

/**
 * This class implements the text book definition of a stack, as described in pg. 233
 * Date: 16/2/22 - 13:19
 */
class TBStack {

  private val s = IntArray(10) // Assume will only need up to 10 spaces

  fun isEmpty(): Boolean {
    return s.top == 0
  }

  fun push(x: Int) {
    s[s.top] = x
    s.top++
  }

  fun pop(): Int {
    if(isEmpty()){
      throw IllegalStateException("underflow")
    }
    s.top--
    return s[s.top]
  }
}
