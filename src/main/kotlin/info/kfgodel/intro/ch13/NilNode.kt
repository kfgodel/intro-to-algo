package info.kfgodel.intro.ch13

import java.lang.UnsupportedOperationException

/**
 * Date: 15/8/20 - 20:48
 */
class NilNode<T : Comparable<T>> : IRBNode<T> {
  override val key: T?
    get() {
      throw UnsupportedOperationException("Nil node doesn't have a key value")
    }
  override var color: RBColor?
    get() = RBColor.BLACK
    set(value) {
      throw UnsupportedOperationException("Nil node can't be changed")
    }
  override var p: IRBNode<T>
    get() {
      throw UnsupportedOperationException("Nil node doesn't have a parent node")
    }
    set(value) {
      throw UnsupportedOperationException("Nil node can't have $value as parent")
    }
  override var right: IRBNode<T>
    get() {
      throw UnsupportedOperationException("Nil node doesn't have a right node")
    }
    set(value) {
      throw UnsupportedOperationException("Nil node can't have $value as right node")
    }
  override var left: IRBNode<T>
    get() {
      throw UnsupportedOperationException("Nil node doesn't have a left node")
    }
    set(value) {
      throw UnsupportedOperationException("Nil node can't have $value as left node")
    }

  override fun toString(): String {
    return "nil"
  }

  override fun childrenCount(): Int {
    return 0
  }
}
