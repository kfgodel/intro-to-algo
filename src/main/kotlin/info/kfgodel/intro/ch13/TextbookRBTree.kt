package info.kfgodel.intro.ch13

/**
 * Date: 03/08/19 - 14:03
 */
class TextbookRBTree<T : Comparable<T>> : RedBlackTree<T> {

  private val nil: IRBNode<T> = NilNode<T>()
  private var root = nil

  override fun insert(key: T): RedBlackTree<T> {
    val z = RBNode(key)
    var y = nil
    var x = root
    while (x !== nil) {
      y = x
      if (z.key!! < x.key!!) {
        x = x.left
      } else {
        x = x.right
      }
    }
    z.p = y
    if (y === nil) {
      root = z
    } else if (z.key!! < y.key!!) {
      y.left = z
    } else {
      y.right = z
    }
    z.left = nil
    z.right = nil
    z.color = RBColor.RED
    insertFixup(z)
    return this
  }

  private fun insertFixup(initialZ: IRBNode<T>) {
    var z = initialZ
    var y: IRBNode<T>
    // Nil checks are not in original algorithm but are necessary to prevent wrong coloring
    while (z.p !== nil && z.p.p !== nil && z.p.color == RBColor.RED) {
      if (z.p === z.p.p.left) {
        y = z.p.p.right
        if (y.color == RBColor.RED) {
          z.p.color = RBColor.BLACK
          y.color = RBColor.BLACK
          z.p.p.color = RBColor.RED
          z = z.p.p
        } else {
          if (z === z.p.right) {
            z = z.p
            leftRotate(z)
          }
          z.p.color = RBColor.BLACK
          z.p.p.color = RBColor.RED
          rightRotate(z.p.p)
        }
      } else {
        y = z.p.p.left
        if (y.color == RBColor.RED) {
          z.p.color = RBColor.BLACK
          y.color = RBColor.BLACK
          z.p.p.color = RBColor.RED
          z = z.p.p
        } else {
          if (z === z.p.left) {
            z = z.p
            rightRotate(z)
          }
          z.p.color = RBColor.BLACK
          z.p.p.color = RBColor.RED
          leftRotate(z.p.p)
        }
      }
    }
    root.color = RBColor.BLACK
  }

  private fun rightRotate(x: IRBNode<T>) {
    val y = x.left
    x.left = y.right
    if (y.right !== nil) {
      y.right.p = x
    }
    y.p = x.p
    if (x.p === nil) {
      root = y
    } else if (x === x.p.right) {
      x.p.right = y
    } else {
      x.p.left = y
    }
    y.right = x
    x.p = y
  }

  private fun leftRotate(x: IRBNode<T>) {
    val y = x.right
    x.right = y.left
    if (y.left !== nil) {
      y.left.p = x
    }
    y.p = x.p
    if (x.p === nil) {
      root = y
    } else if (x === x.p.left) {
      x.p.left = y
    } else {
      x.p.right = y
    }
    y.left = x
    x.p = y
  }

  override fun delete(deletedKey: T): RedBlackTree<T> {
    val z = findNodeFor(deletedKey)
    if(z === nil){
      return this// there's no such key on this tree
    }
    var y = z
    var yOriginalColor = y.color
    val x: IRBNode<T>
    if (z.left === nil) {
      x = z.right
      transplant(z, z.right)
    } else if (z.right === nil) {
      x = z.left
      transplant(z, z.left)
    } else {
      y = minimum(z.right)
      yOriginalColor = y.color
      x = y.right
      if (y.p === z) {
        if(x !== nil){ // This check is not on the book, but prevents assigning a parent to nil
          x.p = y
        }
      } else {
        transplant(y, y.right)
        y.right = z.right
        y.right.p = y
      }
      transplant(z, y)
      y.left = z.left
      y.left.p = y
      y.color = z.color
    }
    if (yOriginalColor == RBColor.BLACK) {
      deleteFixup(x)
    }
    return this
  }

  private fun findNodeFor(deletedKey: T): IRBNode<T> {
    var cursor = root
    while(cursor !== nil){
      if(cursor.key!! == deletedKey){
        break // We found it
      }
      cursor = if(cursor.key!! > deletedKey){
        cursor.left
      }else{
        cursor.right
      }
    }
    return cursor
  }

  override fun size(): Int {
    return root.childrenCount()
  }

  override fun leftCount(): Int {
    return root.left.childrenCount()
  }

  override fun rightCount(): Int {
    return root.right.childrenCount()
  }

  private fun deleteFixup(x: IRBNode<T>) {
    var x = x
    while (x !== root && x.color == RBColor.BLACK) {
      if (x === x.p.left) {
        var w = x.p.right
        if (w.color == RBColor.RED) {
          w.color = RBColor.BLACK
          x.p.color = RBColor.RED
          leftRotate(x.p)
          w = x.p.right
        }
        if (w.left.color == RBColor.BLACK && w.right.color == RBColor.BLACK) {
          w.color = RBColor.RED
          x = x.p
        } else {
          if (w.right.color == RBColor.BLACK) {
            w.left.color = RBColor.BLACK
            w.color = RBColor.RED
            rightRotate(w)
            w = x.p.right
          }
          w.color = x.p.color
          x.p.color = RBColor.BLACK
          w.right.color = RBColor.BLACK
          leftRotate(x.p)
          x = root
        }
      } else {
        var w = x.p.left
        if (w.color == RBColor.RED) {
          w.color = RBColor.BLACK
          x.p.color = RBColor.RED
          rightRotate(x.p)
          w = x.p.left
        }
        if (w.right.color == RBColor.BLACK && w.left.color == RBColor.BLACK) {
          w.color = RBColor.RED
          x = x.p
        } else {
          if (w.left.color == RBColor.BLACK) {
            w.right.color = RBColor.BLACK
            w.color = RBColor.RED
            leftRotate(w)
            w = x.p.left
          }
          w.color = x.p.color
          x.p.color = RBColor.BLACK
          w.left.color = RBColor.BLACK
          rightRotate(x.p)
          x = root
        }
      }
    }
    x.color = RBColor.BLACK
  }

  private fun minimum(node: IRBNode<T>): IRBNode<T> {
    var currentNode = node
    while (currentNode !== nil) {
      if (currentNode.left === nil) {
        break
      }
      currentNode = currentNode.left
    }
    return currentNode
  }

  private fun transplant(u: IRBNode<T>, v: IRBNode<T>) {
    if (u.p === nil) {
      root = v
    } else if (u === u.p.left) {
      u.p.left = v
    } else {
      u.p.right = v
    }
    if(v !== nil){ // Not on the book but prevent assigning a parent to nil
      v.p = u.p
    }
  }

}
