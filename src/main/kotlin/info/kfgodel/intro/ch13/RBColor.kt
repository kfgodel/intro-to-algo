package info.kfgodel.intro.ch13

/**
 * This represents one of the possible colors for a red black tree node
 * Date: 3/8/19 - 15:06
 */
enum class RBColor {
  RED, BLACK
}
