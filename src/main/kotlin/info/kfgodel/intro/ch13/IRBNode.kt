package info.kfgodel.intro.ch13

/**
 * Date: 15/8/20 - 20:47
 */
interface IRBNode<T : Comparable<T>> {
  val key: T?
  var color: RBColor?
  var p: IRBNode<T>
  var right: IRBNode<T>
  var left: IRBNode<T>
  fun childrenCount(): Int
}
