package info.kfgodel.intro.ch13

/**
 * This interface defines the contract that a red black tree needs to fulfil (according to the book ch.16)
 *
 * Date: 03/08/19 - 13:55
 */
interface RedBlackTree<T : Comparable<T>> {
  fun insert(key: T): RedBlackTree<T>
  fun delete(key: T): RedBlackTree<T>
  fun size(): Int
  fun leftCount(): Int
  fun rightCount(): Int
}
