package info.kfgodel.intro.ch13

/**
 * This class represents a single node of the red balck tree
 * Date: 3/8/19 - 15:37
 */
class RBNode<T:Comparable<T>> (override val key: T?) : IRBNode<T> {
  // For non 'nil' nodes
  constructor(key: T, color: RBColor, nil: IRBNode<T>) :this(key) {
    this.color = color
    p = nil
    right = nil
    left = nil
  }

  override var color: RBColor? = null
  override var p: IRBNode<T> = this // Only true for nil or empty root
  override var right: IRBNode<T> = this
  override var left: IRBNode<T> = this

  override fun toString(): String {
    if(p === this){
      return "${color}(${key}) -> self"
    }else{
      return "${color}(${key}) -> ${p}"
    }
  }

  override fun childrenCount(): Int {
    return 1 + left.childrenCount() + right.childrenCount()
  }

}
