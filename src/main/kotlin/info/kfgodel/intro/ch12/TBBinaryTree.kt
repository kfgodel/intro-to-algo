package info.kfgodel.intro.ch12

/**
 * Date: 16/2/22 - 17:38
 */
class TBBinaryTree {
  object NIL : TBBinaryTreeNode(Int.MIN_VALUE)

  private var root: TBBinaryTreeNode = NIL

  fun search(k: Int): TBBinaryTreeNode {
    return searchFrom(root, k)
  }

  private fun searchFrom(x: TBBinaryTreeNode, k: Int): TBBinaryTreeNode {
    if (x == NIL || k == x.key) {
      return x
    }
    if (k < x.key) {
      return searchFrom(x.left, k)
    } else {
      return searchFrom(x.right, k)
    }
  }

  fun minimum(): TBBinaryTreeNode {
    return minimumFrom(root)
  }

  private fun minimumFrom(x: TBBinaryTreeNode): TBBinaryTreeNode {
    var x1 = x
    while (x1.left != NIL) {
      x1 = x1.left
    }
    return x1
  }

  fun maximum(): TBBinaryTreeNode {
    return maximumFrom(root)
  }

  private fun maximumFrom(x: TBBinaryTreeNode): TBBinaryTreeNode {
    var x1 = x
    while (x1.right != NIL) {
      x1 = x1.right
    }
    return x1
  }

  fun successor(x: TBBinaryTreeNode): TBBinaryTreeNode {
    if (x.right != NIL) {
      return minimumFrom(x.right)
    }
    var r = x
    var y = r.p
    while (y != NIL && r == y.right) {
      r = y
      y = y.p
    }
    return y
  }

  fun predecessor(x: TBBinaryTreeNode): TBBinaryTreeNode {
    if (x.left != NIL) {
      return maximumFrom(x.left)
    }
    var l = x
    var y = l.p
    while (y != NIL && l == y.left) {
      l = y
      y = y.p
    }
    return y
  }

  fun insert(z: TBBinaryTreeNode) {
    var y: TBBinaryTreeNode = NIL
    var x = root
    while (x != NIL) {
      y = x
      if (z.key < x.key) {
        x = x.left
      } else {
        x = x.right
      }
    }
    z.p = y
    if (y == NIL) {
      root = z
    } else if (z.key < y.key) {
      y.left = z
    } else {
      y.right = z
    }
  }

  fun delete(z: TBBinaryTreeNode) {
    if(z.left == NIL){
      transplant(z,z.right)
    } else if (z.right == NIL){
      transplant(z, z.left)
    } else {
      val y = minimumFrom(z.right)
      if( y.p != z ){
        transplant(y, y.right)
        y.right = z.right
        y.right.p = y
      }
      transplant(z,y)
      y.left = z.left
      y.left.p = y
    }
  }

  private fun transplant(u: TBBinaryTreeNode, v: TBBinaryTreeNode) {
    if(u.p == NIL){
      root = v
    } else if (u == u.p.left){
      u.p.left = v
    } else {
      u.p.right = v
    }
    if (v != NIL){
      v.p = u.p
    }
  }
}

open class TBBinaryTreeNode(val key: Int) {
  var p: TBBinaryTreeNode = if( TBBinaryTree.NIL != null) TBBinaryTree.NIL else this
  var left: TBBinaryTreeNode = if( TBBinaryTree.NIL != null) TBBinaryTree.NIL else this
  var right: TBBinaryTreeNode = if( TBBinaryTree.NIL != null) TBBinaryTree.NIL else this
}
